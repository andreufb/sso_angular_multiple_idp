# SSO Angular Aplication with EGI Check-in as Identity Provider

Project generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.8.

With the SSO once you are logged into the [EGI Check-in](https://aai.egi.eu/registry) you can access all the resources that this application has and, if previously you have identified yourself in one of the EGI Check-in platforms you don't need to log in in this application to access its resources.

## Steps:

#### 1. Generate a new angular project and install angular CLI
```
ng new project_name
npm install
npm install -g @angular/cli
```

#### 2. Generate a component to protect it

`ng g c component_name`

#### 3. Go to the `app.module.ts` file and import and declare the following modules:
```
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { OAuthModule } from 'angular-oauth2-oidc';
import { HomeComponent } from './home/home.component';
import { MatListModule } from '@angular/material/list';
```

#### 4. Generate a new TypeScript file for configure the authentication process:
The issuer is the Identity Provider, in this case the EGI Check-in development instance. The `AuthConfig` module is also needed.

You always need the issuer, client ID and type of response. Additionally, if you have a client secret you need to pass it, you can configure more options like redirect URL after loggin, the desired scopes and the endpoint where the user info is located.

```
import { AuthConfig } from 'angular-oauth2-oidc';

export const authCodeFlowConfig: AuthConfig = {

    issuer: 'https://aai-dev.egi.eu/oidc/',
    redirectUri: window.location.origin + '/home',
    clientId: 'b21aab5f-8ca2-4186-aaf0-c8abaa11bbca',
    userinfoEndpoint: 'https://aai-dev.egi.eu/oidc/userinfo',
    dummyClientSecret: 'E1gy7lUB_l9ED0qYJqn-kVXllTKsGVb16ZtSQkGkBm3kJ52gJ-6CJwed1h3odw1EcFKPesegFyNra8vQ292DUg',
    responseType: 'code',
    scope: 'openid profile email offline_access',
    showDebugInformation: true
};
```
#### 5. Open the `app-routing.module.ts` and protect the resources/components:
Define the components to protect:
```
const routes: Routes = [
  {path: 'home', component: HomeComponent}
]
```
Import the `RouterModule.forRoot()` and indicate the list of components:
```
import { OAuthService, JwksValidationHandler } from 'angular-oauth2-oidc';
import { authCodeFlowConfig } from './sso.config';
import { HttpClient, HttpHeaders} from '@angular/common/http';

```
#### 6. Put the router outlet in the `app.component.html` and let it work if there is a token:
`<router-outlet *ngIf="token"></router-outlet>`

#### 7. Open the `app.component.ts` and configure as following:
Import the following modules and configuration constant (authCodeFlowConfig):
```
import { Component } from '@angular/core';
import { OAuthService, JwksValidationHandler } from 'angular-oauth2-oidc';
import { authCodeFlowConfig } from './sso.config';
```

Declare the component and the services inside the constructor
```
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  claims;
  id_token;
  access_token;

  constructor(private oauthService: OAuthService) {
    this.configureSingleSignOn();
  }
```

Configure the SSO, the header for the request of the user information and a fucntion to loggout the user:

PD: The loggout function does not revoke the token from the Identity Provider so the user can access the resource aggain.
```
  configureSingleSignOn() {
    this.oauthService.configure(authCodeFlowConfig);

    this.oauthService.tokenValidationHandler = new JwksValidationHandler();
    this.oauthService.loadDiscoveryDocumentAndLogin(); 
  }

  loggout() {
    this.oauthService.logOut()
  }
```

Finally, define the functionality to get the token and claims from the Identity Provider
```

  get token() {

    let claims: any = this.oauthService.getIdentityClaims();
    this.claims = claims
    this.id_token = this.oauthService.getIdToken()
    this.access_token = this.oauthService.getAccessToken()
    return claims ? claims: null;
  }

}

```

#### 8. (OPTIONAL) Get the user additonal information:

Define the header to use the access token getted at the previous step:
```
import { HttpClient, HttpHeaders } from '@angular/common/http';

httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }).set('Authorization', `Bearer ${this.access_token}`)
  };
```
Define the http service and use it to get the user information:
```
constructor(private http: HttpClient) { }

ngOnInit(): void {
    this.http.get('https://aai-dev.egi.eu/oidc/userinfo', this.httpOptions).pipe(retry(1)).subscribe(
      data => {
        this.user_info = JSON.parse(JSON.stringify(data));
    });
  }
```


## Run the project

`ng serve` or `ng serve --host desired_IP`

In case of not indicating the IP navigate to `http://localhost:80/` and, in the case of indicating the IP, nvaigate to `http://desired_IP:80/`.

The app will automatically reload if you change any of the source files.


## Documentation used

OAuthService: https://manfredsteyer.github.io/angular-oauth2-oidc/docs/injectables/OAuthService.html