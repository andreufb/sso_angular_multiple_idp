import { Component } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';
import { JwksValidationHandler } from 'angular-oauth2-oidc-jwks';
import { EGI_authConfig, EMSO_authConfig } from './sso.config';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'sso-app';
  access_time;
  claims;
  id_token;
  access_token;
  provider = localStorage.getItem('provider')


  constructor(private oauthService: OAuthService, private http: HttpClient, private router: Router) {
    var a = localStorage.getItem('provider')
    if (a == 'EGI') {
      this.EGI();
    }
    else {
      if (a == 'EMSO') {
        this.EMSO();
      }
    }
  }

  EGI() {
    this.oauthService.configure(EGI_authConfig);
    this.oauthService.tokenValidationHandler = new JwksValidationHandler();
    this.oauthService.loadDiscoveryDocumentAndLogin();
  };

  EMSO() {
    this.oauthService.configure(EMSO_authConfig);
    this.oauthService.tokenValidationHandler = new JwksValidationHandler();
    this.oauthService.loadDiscoveryDocumentAndLogin();
  };


  login(provider) {
    localStorage.setItem('provider', provider)
    window.location.assign(window.location.href)
    
  };


  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
    })
  };


  logout() {
    this.oauthService.revokeTokenAndLogout();
    this.oauthService.logOut();
    localStorage.removeItem('provider')
    window.location.assign('http:\\' + window.location.hostname + ':4200/')
  }



  get token() {
    let claims: any = this.oauthService.getIdentityClaims();
    this.claims = claims
    this.access_time = this.oauthService.getIdTokenExpiration();
    this.id_token = this.oauthService.getIdToken()
    this.access_token = this.oauthService.getAccessToken()
    return claims ? claims : null;
  }


}

