import { AuthConfig } from 'angular-oauth2-oidc';

export const EGI_authConfig: AuthConfig = {

    issuer: 'https://aai-dev.egi.eu/oidc/',
    redirectUri: window.location.origin + '/home',
    clientId: 'b21aab5f-8ca2-4186-aaf0-c8abaa11bbca',
    userinfoEndpoint: 'https://aai-dev.egi.eu/oidc/userinfo',
    dummyClientSecret: 'E1gy7lUB_l9ED0qYJqn-kVXllTKsGVb16ZtSQkGkBm3kJ52gJ-6CJwed1h3odw1EcFKPesegFyNra8vQ292DUg',
    responseType: 'code',
    scope: 'openid profile email offline_access eduperson_entitlement',
    showDebugInformation: true
};

export const EMSO_authConfig: AuthConfig = {

    // Url of the Identity Provider
    issuer: 'https://steyer-identity-server.azurewebsites.net/identity',
  
    // URL of the SPA to redirect the user to after login
    redirectUri: window.location.origin + '/home',
  
    // The SPA's id. The SPA is registerd with this id at the auth-server
    clientId: 'spa-demo',
  
    // set the scope for the permissions the client should request
    // The first three are defined by OIDC. The 4th is a usecase-specific one
    scope: 'openid profile email',
};

export const Google_authConfig: AuthConfig = {

    // Url of the Identity Provider
    issuer: 'https://accounts.google.com',
  
    // URL of the SPA to redirect the user to after login
    redirectUri: window.location.origin + '/home',
  
    // The SPA's id. The SPA is registerd with this id at the auth-server
    clientId: '1004270452653-m396kcs7jc3970turlp7ffh6bv4t1b86.apps.googleusercontent.com',
  
    strictDiscoveryDocumentValidation: false,
  
    // set the scope for the permissions the client should request
    // The first three are defined by OIDC. The 4th is a usecase-specific one
    scope: 'openid profile email',
  
    showDebugInformation: true,
  }