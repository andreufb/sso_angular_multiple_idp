import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { formatDate } from '@angular/common';
import { AppComponent } from '../app.component';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  payload;

  in = 0;

  constructor(public appvals: AppComponent, private http: HttpClient) {
/*     this.http.get('https://aai-dev.egi.eu/oidc/userinfo', this.httpOptions).pipe(retry(1)).subscribe(
      data => {
        this.user_info = JSON.parse(JSON.stringify(data));
        this.vo = JSON.stringify(this.user_info.eduperson_entitlement).split(':')

        
        for (let entry of this.vo) {
          if (entry == 'registry') {
            this.vo_total += this.vo[this.in+2].split('#', 1) + ' of ' + this.vo[this.in+1] + ' ; '
          }

          this.in += 1
        }
    }); */
   }

  id_token = this.appvals.id_token
  access_token = this.appvals.access_token
  /* access_time = formatDate(this.appvals.access_time, 'hh:mm:ss', 'en-US') */
  claims = this.appvals.claims
  user_info;
  vo;

/*   vo_total = ''
  date = formatDate(this.appvals.claims.auth_time*1000, 'dd/MM/yyyy', 'en-US')
  response */
  
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }).set('Authorization', `Bearer ${this.access_token}`)
  };

  ngOnInit(): void {
    
  }

  async getPerson() {
    await new Promise ((res) => {
      this.http.get('http://aai-dev.emso.eu:5000/api/person/', this.httpOptions).pipe(retry(1)).subscribe(
      data => {
        this.payload = JSON.parse(JSON.stringify(data));
        res()
      });
    });
    return this.payload

  };

  get_person() {
    return new Promise((resolve) => {
      resolve(this.getPerson())
    })
  }

  async getPhysicalSystem() {
    await new Promise ((res) => {
      this.http.get('http://aai-dev.emso.eu:5000/api/physical-system/', this.httpOptions).pipe(retry(1)).subscribe(
      data => {
        this.payload = JSON.parse(JSON.stringify(data));
        res()
      });
    });
    return this.payload

  };

  get_physical_system() {
    return new Promise((resolve) => {
      resolve(this.getPhysicalSystem())
    })
  }

  async getMission() {
    await new Promise ((res) => {
      this.http.get('http://aai-dev.emso.eu:5000/api/mission/', this.httpOptions).pipe(retry(1)).subscribe(
      data => {
        this.payload = JSON.parse(JSON.stringify(data));
        res()
      });
    });
    return this.payload

  };

  get_mission() {
    return new Promise((resolve) => {
      resolve(this.getMission())
    })
  }

/*   httpOptionss = {
    headers: new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
    }).set('Authorization', `Basic czZCaGRSa3F0MzpnWDFmQmF0M2JW`)
  };


  logout() {
    this.http.post('https://aai-dev.egi.eu/oidc/revoke', `token=${this.access_token}` , this.httpOptionss).pipe(retry(1)).subscribe(
      data => {
        window.alert(JSON.parse(JSON.stringify(data)));
    });
  } */

  objectKeys = Object.keys;

}
